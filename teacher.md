# 教師用マニュアル

## 授業用のgitリポジトリの作成

### リポジトリのfork

1. <https://gitlab.com> にアカウントがなければ，アカウントを作成する．以下アカウント名を `xxxxx` とする．
2. <https://gitlab.com> にログインする．
3. <https://gitlab.com/ku-istc/gitpod-test> を開き，右上の「Fork」ボタンを押し，自分のアカウントを選択する．
4. forkされた自分のリポジトリが開く．
   - "Settings" / "General" / "Visibility, project features, permissions" の右のExpandをクリックして，
     "Project visivility"を"Public"にし，"Save Changes"ボタンをクリックする．

### GitPodを開く

1. 以下のURLの`https://gitlab.com/xxxxx/gitpod-test`の部分を自分のリポジトリのURLに変更して開く
   (`xxxxx`が自分のアカウント名)．
   - <https://gitpod.io/#https://gitlab.com/xxxxx/gitpod-test>
2. "Login with GitLab & Lauch Workspace"をクリックする (GitPodの画面)．
3. 以下のボタンが表示されたらクリックする．
   - "Authorize"ボタン (GitLabの画面)
   - "Accept Terms"ボタン (GitPodの画面)
   - "Grant Access"ボタン (GitPodの画面)
4. GitPodのworkspaceが表示される．
5. 別Windowで <https://gitpod.io/access-control/> を開き，
   GitLabの"repository access"にチェックをつけて"Update"ボタンをクリックする．
   - これでGitPodから自分のGitLabのリポジトリにpushできるようになる．

### プログラムの修正とpush

1. GitPodでプログラムを修正する．
2. Visual Studio Codeと同様の操作で自分のGitLabリポジトリに修正をpushする．
